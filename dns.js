const dns2 = require('dns2');
const { Packet } = dns2;

const answers = {
    data: [],
    add(a) {
        this.data.push(a)
    },
    remove(a) {
        this.data = this.data.filter((b) => b!==a)
    }
}

const server = dns2.createUDPServer((request, send, rinfo) => {
    const response = Packet.createResponseFromRequest(request);
    const [ question ] = request.questions;
    const { name } = question;
    response.answers = answers.data
    send(response);
});


server.on('request', (request, response, rinfo) => {
    console.log(request.header.id, request.questions[0])
    console.log(answers.data)
});

server.listen(53)


module.exports = { Packet, answers }