const CONFIG = require('./config')
const express = require('express');
const digio = require('./digitalocean')
const { v4: uuid } = require('uuid')
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http, {
    cors: {
        origin: "http://localhost:8080",
        methods: ["GET", "POST"]
    }
});

app.use(express.static('vue/dist/'))

sessionStore = {
    find(id) {
        return this.sessions.find((s => s.id === id))
    },
    new() {
        let session = {
            id: uuid(),
            status: 0
        }
        this.sessions.push(session)
        return session
    },
    findOrCreate(id) {
        if (id && this.find(id)){
            return this.find(id)
        } else {
            return this.new()
        }
    },
    removeByID(id) {
        if (id && this.find(id)){
            this.sessions = this.sessions.filter(value => value.id !== id)
        }
    },
    refreshClient(id) {
        io.to(id).emit('refreshClient', this.find(id))
    },
    refreshAdmins() {
        console.log(`New request list: ${JSON.stringify(this.sessions)}`)
        io.to('admin').emit('updateRequests', this.sessions)
    },
    sessions: []
}

io.use((socket, next) => {
    socket.sessionId = sessionStore.findOrCreate(socket.handshake.auth.id).id
    socket.join(socket.sessionId)
    if (socket.sessionId !== socket.handshake.auth.id) {
        socket.emit('setSession', socket.sessionId)
    }
    if (socket.id !== sessionStore.find(socket.sessionId).tempId) {
        console.log('refresh')
        new Promise(resolve => setTimeout(resolve, 20)).then(() => {

            sessionStore.refreshClient(socket.sessionId)
        })
        sessionStore.find(socket.sessionId).tempId = socket.id
    }
    console.log(socket.rooms)
    return next()
});

io.on('connection', (socket) => {
    socket.on('sudo', (msg, callback) => {
        if (msg === CONFIG.server.password) {
            socket.join('admin')
            callback(true);
            socket.emit('updateRequests', sessionStore.sessions)
        }
        else {
            callback(false);
        }
    })

    socket.on('acceptRequest', (msg) => {
        if (socket.rooms.has('admin')){
            if (sessionStore.find(msg)){
                sessionStore.find(msg).status = 2
                sessionStore.refreshAdmins()
                sessionStore.refreshClient(msg)
                new Promise(resolve => setTimeout(resolve, 2000)).then(() => {
                    sessionStore.find(msg).status = 3
                    sessionStore.refreshAdmins()
                    sessionStore.refreshClient(msg)
                        new Promise(resolve => setTimeout(resolve, 2000)).then(() => {
                            sessionStore.find(msg).status = 4
                            sessionStore.find(msg).password = "A beautiful password"
                            sessionStore.find(msg).machine = "My Nice Machine"
                            sessionStore.find(msg).address = "The Magnificient Address"
                            sessionStore.refreshAdmins()
                            sessionStore.refreshClient(msg)
                        })
                })
            }
        }
    })

    socket.on('declineRequest', (msg) => {
        if (socket.rooms.has('admin')){
            sessionStore.find(msg).status = -1
            sessionStore.refreshClient(msg)
            sessionStore.refreshAdmins()
        }
    })

    socket.on('requestHosting', (msg) => {
        if (sessionStore.find(socket.sessionId).status <= 0) {
            sessionStore.find(socket.sessionId).status = 1
            sessionStore.find(socket.sessionId).user = msg
            sessionStore.refreshClient(socket.sessionId)
            sessionStore.refreshAdmins()
        }
    })

    socket.on('disconnect', (data) =>{
        delete sessionStore.find(socket.sessionId)?.tempId
    })
});

http.listen(CONFIG.server.port, () => {
    console.log(`listening on *:${CONFIG.server.port}`);
});