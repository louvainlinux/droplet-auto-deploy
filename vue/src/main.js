import Vue from 'vue'
import App from './App.vue'
import io from 'socket.io-client'


Vue.prototype.$io = io({ autoConnect: false });
if (localStorage.getItem('session')){
    Vue.prototype.$io.auth = {id: localStorage.getItem('session')}
}
Vue.prototype.$io.connect(() => {
    console.log(Vue.prototype.$io.connected)
})

Vue.prototype.$io.on('setSession', (msg) => {
    Vue.prototype.$io.auth = {id: msg}
    localStorage.setItem('session', msg)
})

Vue.prototype.$io.onAny((event, args) => {
    console.log(`${event}: ${JSON.stringify(args)}`)
})

// Vue.prototype.$io.on('disconnect', () => {
//     window.location.reload(true)
// })

new Vue({
    el: '#app',
    render: h => h(App)
})