const fs = require('fs')
const yaml = require('yaml')

const CONFIG = yaml.parse(fs.readFileSync('./config.yaml', 'utf-8'))
module.exports = CONFIG