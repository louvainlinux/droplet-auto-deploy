## VPS auto deploy
Ce programme permet de créer à la volée des VPS DigitalOcean (appelés Droplets) préconfigurés pour les activités du Louvain-li-Nux, afin que chaque participant ai accès à un environnement Linux isolé le temps d'une activité (et les droplets étant facturé à l'heure (0.007$/h/droplet pour les moins chers), le coût pour une activité reste négligeable).

De plus, un sous nom de domaine est créé dynamiquement pour chaque machine.
## Comment utiliser ?


### Configuration
Veuillez créer un fichier `do-keys` contenant votre clé d'API DigitalOcean (obtenable [ici](https://cloud.digitalocean.com/account/api/tokens)).

Les autres options utilisables sont dans le fichier `config.yaml`