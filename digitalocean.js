const fs = require('fs')
const fetch = require('node-fetch')
const dns = require('./dns')
const token = fs.readFileSync('./do-keys', 'utf-8')
const apiPath = 'https://api.digitalocean.com/v2/'
const CONFIG = require('./config')

dns_index = 1

function generateSubDomain(domain, ip) {
    let subdomain = `${dns_index}.${domain}`
    dns_index ++
    return subdomain
}

function addSubDomain(domain, ip) {
    dns.answers.add({
        name: domain,
        type: dns.Packet.TYPE.A,
        class: dns.Packet.CLASS.IN,
        ttl: 300,
        address: ip
    })
}

function newDroplet() {
    let domain = generateSubDomain(CONFIG.droplets.domain)
    return request("droplets", {
        method: 'POST',
        body: {
            name:       domain,
            region:     CONFIG.droplets.region,
            size:       CONFIG.droplets.size,
            image:      CONFIG.droplets.image_id,
            ssh_keys:   CONFIG.droplets.root_ssh_keys,
            tags:       CONFIG.droplets.tags
    }}).then(r => {
        console.log(r.droplet)
        return r.droplet
    })
}

function getDroplets() {
    return request("droplets", {method: 'GET'})
}

function request(url, params) {
    if (params == null) { params = {} }
    if (!params.headers) { params.headers = {} }
    params.headers = Object.assign(
        {Authorization: `Bearer ${token}`},
        params.headers
    )
    if (params.body) {
        if (params.body.constructor.name === 'Object') {
            params.headers['Content-Type'] = 'application/json'
            params.body = JSON.stringify(params.body)
        }
    }
    url = apiPath + url
    console.log(`API call: ${url} : ${JSON.stringify(params)}`)
    return fetch(url, params)
        .then(res => {
            if (! res.ok) {
                return res.json().then(json => {
                    throw {type: 'server', e: json}
                })

            }
            if ((res.headers.get("Content-Type") || "").startsWith("application/json")) {
                return res.json().then(json => {
                    return json
                })
            } else {
                return res.blob().then()
            }
        })
        .catch((e) => { //Fetch error
            if (e.type === 'server') {
                throw e
            }
            throw {type: 'network', e: e}
        })

}


setInterval(() => {
    getDroplets().then(droplets => {
        droplets.map(d => `${d.name}, ${status}`).forEach((d) => console.log(`- ${d}`))
})}, 5000)


module.exports = {
    newDroplet,
    getDroplets
}